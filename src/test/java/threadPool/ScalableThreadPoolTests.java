package threadPool;

import org.junit.Test;
import treadPool.model.ScalableThreadPool;

import static threadPool.utils.SomeTask.someTask;

public class ScalableThreadPoolTests {

    @Test
    public void scalableThreadPoolTest_15_1() throws InterruptedException {
        ScalableThreadPool threadPool = new ScalableThreadPool(15, 1);
        threadPool.start();
        threadPool.execute(someTask(1000, 1));
        threadPool.execute(someTask(500, 2));
        threadPool.execute(someTask(500, 3));
        threadPool.execute(someTask(500, 4));
        threadPool.execute(someTask(1000, 5));
        threadPool.execute(someTask(500, 6));
        threadPool.execute(someTask(1000, 7));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));

        Thread.sleep(2000);
        System.exit(0);
    }

    @Test
    public void scalableThreadPoolTest_10_5() throws InterruptedException {
        ScalableThreadPool threadPool = new ScalableThreadPool(10, 5);
        threadPool.start();
        threadPool.execute(someTask(1000, 1));
        threadPool.execute(someTask(500, 2));
        threadPool.execute(someTask(500, 3));
        threadPool.execute(someTask(500, 4));
        threadPool.execute(someTask(1000, 5));
        threadPool.execute(someTask(500, 6));
        threadPool.execute(someTask(1000, 7));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));

        Thread.sleep(2000);
        System.exit(0);
    }
}
