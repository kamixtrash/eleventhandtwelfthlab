package threadPool.utils;

public class SomeTask {

    public static Runnable someTask(int delay, int param) {
        return() -> {
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(param + " " + Thread.currentThread().getName());
        };
    }
}
