package threadPool;

import org.junit.Test;
import treadPool.model.FixedThreadPool;

import static threadPool.utils.SomeTask.someTask;

public class FixedThreadPoolTests {
    @Test
    public void fixedThreadPoolTest_10() throws InterruptedException {
        FixedThreadPool threadPool = new FixedThreadPool(10);
        threadPool.start();
        threadPool.execute(someTask(1000, 1));
        threadPool.execute(someTask(500, 2));
        threadPool.execute(someTask(500, 3));
        threadPool.execute(someTask(500, 4));
        threadPool.execute(someTask(1000, 5));
        threadPool.execute(someTask(500, 6));
        threadPool.execute(someTask(1000, 7));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));

        Thread.sleep(2000);
        System.exit(0);
    }

    @Test
    public void fixedThreadPoolTest_5() throws InterruptedException {
        FixedThreadPool threadPool = new FixedThreadPool(5);
        threadPool.start();
        threadPool.execute(someTask(1000, 1));
        threadPool.execute(someTask(500, 2));
        threadPool.execute(someTask(500, 3));
        threadPool.execute(someTask(500, 4));
        threadPool.execute(someTask(1000, 5));
        threadPool.execute(someTask(500, 6));
        threadPool.execute(someTask(1000, 7));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));
        threadPool.execute(someTask(500, 8));

        Thread.sleep(3000);
        System.exit(0);
    }
}
