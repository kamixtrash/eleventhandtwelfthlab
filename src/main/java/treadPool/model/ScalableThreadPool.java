package treadPool.model;

import treadPool.utils.Worker;
import treadPool.utils.WorkerType;

import java.util.*;

public class ScalableThreadPool implements ThreadPool {

    private final int maxThreadsAmount;
    private final int minThreadsAmount;
    private final Object lock = new Object();
    private final TasksQueue tasks = new TasksQueue();

    private volatile int threadsCounter;

    private final List<Worker> workers = new ArrayList<>();

    public ScalableThreadPool(int maxThreadsCount, int minThreadsCount) {
        this.maxThreadsAmount = maxThreadsCount;
        this.minThreadsAmount = minThreadsCount;
    }

    public void interrupt() {
        workers.forEach(Thread::interrupt);
    }

    private void createAndStartWorker() {
        Worker worker = new Worker(lock, tasks, WorkerType.SCALABLE);
        workers.add(worker);
        worker.start();
        threadsCounter++;
    }

    @Override
    public void start() {
        for (int i = 0; i < minThreadsAmount; i++) {
            createAndStartWorker();
        }
    }

    @Override
    public void execute(Runnable task) {
        synchronized (lock) {
            tasks.add(task);

            lock.notify();
        }
    }

    public class TasksQueue {
        private final Queue<Runnable> tasks = new ArrayDeque<>();

        public void add(Runnable task) {
            tasks.add(task);

            if (tasks.size() > 1 && threadsCounter < maxThreadsAmount) {
                createAndStartWorker();
            }
        }

        public Runnable poll() {
            return tasks.poll();
        }

        private void deleteWorkers() {
            for (ListIterator<Worker> iterator = workers.listIterator(); iterator.hasNext();) {
                Worker worker = iterator.next();
                if (worker.isWaitingNewTask()) {
                    worker.interrupt();
                    iterator.remove();
                    threadsCounter--;
                    return;
                }
            }
        }

        public boolean isEmpty() {
            if (tasks.isEmpty() && threadsCounter > minThreadsAmount) {
                deleteWorkers();
            }
            return tasks.isEmpty();
        }
    }
}
