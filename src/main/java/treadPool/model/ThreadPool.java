package treadPool.model;

public interface ThreadPool {
    void start();

    void execute(Runnable task);
}
