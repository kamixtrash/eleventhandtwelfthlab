package treadPool.model;

import treadPool.utils.Worker;
import treadPool.utils.WorkerType;

import java.util.ArrayDeque;
import java.util.Queue;

public class FixedThreadPool implements ThreadPool {

    private final Object lock = new Object();
    private final Queue<Runnable> tasks = new ArrayDeque<>();
    private final int threadsCount;
    private final Worker[] workersPool;

    public FixedThreadPool(int threadsCount) {
        this.threadsCount = threadsCount;
        workersPool = new Worker[threadsCount];
    }

    @Override
    public void start() {
        for (int i = 0; i < threadsCount; i++) {
            workersPool[i] = new Worker(lock, tasks, WorkerType.FIXED);
            workersPool[i].start();
        }
    }

    @Override
    public void execute(Runnable task) {
        synchronized (lock) {
            tasks.add(task);
            lock.notify();
        }
    }
}
