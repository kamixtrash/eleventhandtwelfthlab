package treadPool.utils;

import treadPool.model.ScalableThreadPool;

import java.util.Queue;

public class Worker extends Thread {

    private final Object lock;
    private Queue<Runnable> tasks;
    private ScalableThreadPool.TasksQueue scalableTasks;
    private final WorkerType type;
    private boolean waitingNewTask;

    public Worker(Object lock, Queue<Runnable> tasks, WorkerType type) {
        this.lock = lock;
        this.tasks = tasks;
        this.type = type;
    }

    public Worker(Object lock, ScalableThreadPool.TasksQueue tasks, WorkerType type) {
        this.lock = lock;
        this.scalableTasks = tasks;
        this.type = type;
    }

    public boolean isWaitingNewTask() {
        if (type.equals(WorkerType.FIXED)) {
            throw new UnsupportedOperationException("Worker in fixed thread pool can't have waiting tasks.");
        }
        return waitingNewTask;
    }

    public void run() {
        while (true) {
            if (Thread.interrupted()) {
                return;
            }
            Runnable task;
            synchronized (lock) {
                if (type.equals(WorkerType.FIXED)) {
                    while (tasks.isEmpty()) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    task = tasks.poll();
                } else {
                    while (scalableTasks.isEmpty()) {
                        try {
                            waitingNewTask = true;
                            lock.wait();
                        } catch (InterruptedException e) {
                            System.out.println("Completed task. " + Thread.currentThread().getName());
                            return;
                        }
                    }
                    waitingNewTask = false;
                    task = scalableTasks.poll();
                }
            }
            execute(task);
        }
    }

    private void execute(Runnable task) {
        if (task != null) {
            try {
                task.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}